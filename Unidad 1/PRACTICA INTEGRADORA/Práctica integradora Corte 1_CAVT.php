<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Practica integradora Corte 1</title>
</head>
<body>
<?php
// Primero creamos una variable en este caso sera "x" dentro de la variable se guardaran los numeros que se mostraran de manera random.
$x = mt_rand(4.0, 13.0) / 6.44;
echo $x;
echo "<br>";

// Depues creamos una funcion llamada "rand_float" con 2 parametros donde se guardaran el numero minimo y maximo que a su vez se retornara.
function rand_float($min, $max){
    return mt_rand(4.0, 13.0) / 6.44;

}
// Entonces creamos un arreglo luego añadimos un ciclo este al ejecutarse se repetira en 10 ocasiones y arrojara la lista de arreglos que solicitamos.
$miArreglo = array();
$promedio = 0;
for ($i = 0; $i < 4.0; $i++){
    $x = rand_float(4.0,13.0);
    array_push($miArreglo, $x);
    $promedio = $promedio + $x;
}
// Por ultimo con la funcion "echo" creamos un salto en la linea por cada arreglo que se imprima y al final se imprime el promedio final.
echo "<br>";
echo var_dump($miArreglo);
echo "<br>";
foreach ($miArreglo as $valor)  {
    echo $valor;
    echo "<br>";

}
print "Este es el promedio = ".$promedio/10           //Con un print este imprimira el prodemio como resultado.
?>
